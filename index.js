let koa = require('koa');
let app = module.exports = new koa();

let getTodo = require('./todo/todo');
let Pug = require('koa-pug');

let todoPug = new Pug({ viewPath: './todo/' });
let hiPug = new Pug({ viewPath: './hi/' });

app.use(ctx => {

    let url = ctx.url;

    if (url === '/') {
        ctx.body = 'Hello world!';
    }
    else if (url === '/hi') {
        ctx.body = hiPug.render('hi');
    }
    else if (url === '/todo') {
        let todos = [];
        todos = getTodo();
        ctx.body = todoPug.render('todo', { todoList: todos });
    }
    else {
        ctx.body = url.split('/')[1];
    }
});

app.listen(process.env.PORT || 8080);

//https://murmuring-fortress-90801.herokuapp.com/
