let koa = require('koa');
let app = module.exports = new koa();

let getTodo = require('./todo/todo');
let Pug = require('koa-pug');

// Test: binding to ctx like ctx.somePug
let somePug = new Pug();
app.context.somePug = somePug; 

let todoPug = new Pug({ viewPath: './todo/' });
let hiPug = new Pug({ viewPath: './hi/' });

// app.use(function *(){
//     //this.response.body
//     this.body = 'Hello from koajs';
// });

app.use(ctx => {
    // ctx.body = 'Hello world';
    // console.log(ctx);
    // let url = ctx.url;
    // if (url === '/date') {
    //     ctx.body = new Date();
    // }
    // else {
    //     ctx.status = 404;
    // // http://koajs.com/#response
    //     ctx.body = 'Sorry friend, I don\'t know what yo want'
    // }
    // // https://www.youtube.com/watch?v=Div1km7DQrI

    let url = ctx.url;

    if (url === '/') {
        ctx.body = 'Hello world!';
    }
    else if (url === '/pug') {
        ctx.body = ctx.somePug.render('h1 Hello, #{name}', { name: 'Man' }, { fromString: true })
    }
    else if (url === '/hi') {
        // ctx.body = 'Hi!';
        ctx.body = hiPug.render('hi');
    }
    else if (url === '/todo') {
        let todos = [];
        todos = getTodo();
        ctx.body = todoPug.render('todo', { todoList: todos });
        // todo - назва
    }
    else {
        ctx.body = url.split('/')[1];;
    }
});

app.listen(process.env.PORT || 8080);

// npm install koa--save
// Pug for Koa
// npm install --save pug koa-pug


//Routing
// var koa = require('koa');
// var router = require('koa-router');
// var app = koa();

// var _ = router(); //Instantiate the router

// _.get('/hello', getMessage); // Define routes

// function* getMessage() {
//     this.body = "Hello world!";
// };

//https://murmuring-fortress-90801.herokuapp.com/
