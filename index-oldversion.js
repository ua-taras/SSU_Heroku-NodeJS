
var http = require('http');
let path;

http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    path = request.url.split('/')[1];
    
    if (path == '') {
        text = 'Hello world!';
    }
    else if (path == 'hi') {
        text = 'Hi!';
    }
    else {
        text = path;
    }
    response.write(text);
    response.end("\nSuccess!");

    // https://code.tutsplus.com/tutorials/understanding-expressjs-routing--cms-29738
    // Express routing

}).listen(process.env.PORT || 8080);

//https://murmuring-fortress-90801.herokuapp.com/
